<?php

// Just return the given data
echo "Dewpoint:\t" . $_POST['dpnt'] . " F\n";
echo "Pressure:\t" . $_POST['pres'] . " inHg\n";
echo "Temperature:\t" . $_POST['temp'] . " F\n";
echo "Visibility:\t" . $_POST['vis'] . " Nautical Miles\n";
echo "Wind Direction:\t" . $_POST['wdir'] . " Deg\n";
echo "Wind Speed:\t" . $_POST['wspd'] . " Knots\n";

?>