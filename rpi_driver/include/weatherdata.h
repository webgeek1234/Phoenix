//
// weatherdata.h
// by Aaron Kling
// ~~~~~~~~~~~~~~~
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef WEATHERDATA_H
#define WEATHERDATA_H

#include "config.h"
#include <string>
#include <boost/asio.hpp>

class WeatherData
{
    public:
        WeatherData(Config *paramConfig);
        ~WeatherData();

        // Accessor functions for the weather data
        int         getDewpoint()      { return iDewPoint; }
        float       getPressure()      { return fPressure; }
        int         getTemperature()   { return iTemperature; }
        int         getVisibility()    { return iVisibility; }
        int         getWindDirection() { return iWindDirection; }
        int         getWindSpeed()     { return iWindSpeed; }
        std::string getRecTime()       { return sTime; }
        std::string getMetarTime();
        bool        hasNewMetar()      { return bHasNewMetar; }

        // Initialize the weather data class
        int  initialize();

        // Get a set of weather values
        void updateData();

    protected:


    private:
        // The current weather values
        int         iDewPoint;
        float       fPressure;
        int         iTemperature;
        int         iVisibility;
        int         iWindDirection;
        int         iWindSpeed;
        std::string sTime;
        std::string sMetarTime;
        bool        bHasNewMetar;

        // Boost serial class
        class SimpleSerial
        {
        public:
            SimpleSerial(std::string port, unsigned int baud_rate) : io(), serial(io,port)
            {
                serial.set_option(boost::asio::serial_port_base::baud_rate(baud_rate));
            }

            void writeString(std::string s)
            {
                boost::asio::write(serial,boost::asio::buffer(s.c_str(),s.size()));
            }

            std::string readLine()
            {
                //Reading data char by char, code is optimized for simplicity, not speed
                using namespace boost;
                char c;
                std::string result;
                for(;;)
                {
                    asio::read(serial,asio::buffer(&c,1));
                    switch(c)
                    {
                        case '\r':
                            break;
                        case '\n':
                            return result;
                        default:
                            result+=c;
                    }
                }
            }

        private:
            boost::asio::io_service io;
            boost::asio::serial_port serial;
        };

        // A pointer to the serial class
        SimpleSerial *ssWeather;

        // A pointer to the config class
        Config *config;

        // Set up and destroy the UART connection
        int   initializeUART();
        void  destructUART();
};

#endif // WEATHERDATA_H
