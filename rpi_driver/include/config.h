//
// config.h
// by Aaron Kling
// ~~~~~~~~~~~~~~~
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef CONFIG_H
#define CONFIG_H

#include <string>
#include <boost/thread.hpp>
#include <boost/asio.hpp>

// Class for receiving connection info from the setup program
class conInfoSetup
{
    private:
        std::string strSetupHost;
        std::string strSetupPort;

    public:
        template <typename Archive>
        void serialize(Archive& ar, const unsigned int version)
        {
            ar & strSetupHost;
            ar & strSetupPort;
        }

        std::string getSetupHost() { return strSetupHost; }
        std::string getSetupPort() { return strSetupPort; }
        void setSetupHost(std::string strNew) { strSetupHost = strNew; }
        void setSetupPort(std::string strNew) { strSetupPort = strNew; }
};

typedef struct
{
    std::string sServer;
    std::string sPage;
    int         iID;
    bool        bDebug;
    std::string sSource;
    std::string sDevice;
    std::string sDevName;

    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & sServer;
        ar & sPage;
        ar & iID;
        ar & bDebug;
        ar & sSource;
        ar & sDevice;
        ar & sDevName;
    }
} structConfig;

class Config
{
    public:
        Config() {}
        virtual ~Config() {}

        // Accessor functions for the config variables
        std::string getServer   () { return sServer; }
        std::string getPage     () { return sPage; }
        int         getID       () { return iID; }
        bool        isDebug     () { return bDebug; }
        std::string getSource   () { return sSource; }
        std::string getDevice   () { return sDevice; }
        std::string getDevName  () { return sDevName; }
        std::string getMetarLoc () { return sMetarFile; }
        bool        isNeedsMetar() { return bNeedsMetar; }
        bool        isTempReal  () { return bTemperature; }
        bool        isDewPReal  () { return bDewPoint; }
        bool        isPresReal  () { return bPressure; }
        bool        isVisiReal  () { return bVisibility; }
        bool        isWinDReal  () { return bWindDirection; }
        bool        isWinSReal  () { return bWindSpeed; }

        // Initialize the config class
        int  initialize(int argc, char** argv);

        // Listen for config changes
        void listenConfig();

        // Update config when something received from listening
        void updateConfig();

        // Write config file based on previously set config values
        void writeConfig();

        // Mutex to lock the updating of the config
        boost::shared_mutex smConfig;
    protected:

    private:
        // The config values
        std::string sServer;
        std::string sPage;
        int         iID;
        bool        bDebug;
        std::string sSource;
        std::string sDevice;
        std::string sDevName;
        std::string sMetarFile;

        // Where the weather values are coming from. (true for sensor, false for METAR)
        bool bNeedsMetar;
        bool bTemperature;
        bool bDewPoint;
        bool bPressure;
        bool bVisibility;
        bool bWindDirection;
        bool bWindSpeed;

        // Multicast receiver class
        class receiver
        {
            public:
            receiver(boost::asio::io_service& io_service, const boost::asio::ip::address& listen_address,
                     const boost::asio::ip::address& multicast_address);

            void read(int iID, conInfoSetup *setupInfo = NULL);
            bool isConfiging() { return bIsConfiging; }
            void setIsConfiging(bool bNew) { bIsConfiging = bNew; }

            private:
              boost::asio::ip::udp::socket socket_;
              boost::asio::ip::udp::endpoint sender_endpoint_;
              enum { max_length = 1024 };
              char data_[max_length];
              bool bIsConfiging;
        };
};

#endif // CONFIG_H
