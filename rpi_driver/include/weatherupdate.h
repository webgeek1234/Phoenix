//
// weatherupdate.h
// by Aaron Kling
// ~~~~~~~~~~~~~~~
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#ifndef WEATHERUPDATE_H
#define WEATHERUPDATE_H

#include "weatherdata.h"
#include "config.h"
#include <string>

class WeatherUpdate
{
    public:
        WeatherUpdate() {}
        virtual ~WeatherUpdate() {}

        // Send the weather data to the updater web page
        int         send(WeatherData *currentData, Config *config);
        int         sendupdate(WeatherData *currentData, Config *config, std::string sPost);
        std::string getPostString(WeatherData *data, Config *config, bool isMetar);

    protected:

    private:
};

#endif // WEATHERUPDATE_H
