//
// rec_config.cpp
// by Aaron Kling
// ~~~~~~~~~~~~~~~
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include "config.h"
#include <fstream>
#include <boost/program_options.hpp>
#include <boost/archive/text_iarchive.hpp>
#include <boost/archive/text_oarchive.hpp>
#include <boost/serialization/string.hpp>
#include <boost/asio.hpp>

namespace po = boost::program_options;

// Initialize the config class
int Config::initialize(int argc, char** argv)
{
    // Setup config stuff
    po::options_description desc("Allowed options");
    desc.add_options()
        ("help",                                                                     "produce help message")
        ("aws_debug,d",      po::value<bool>()->default_value(false),                "display debugging info")
        ("aws_device,e",     po::value<std::string>()->default_value("unk"),         "device file where station is attached")
        ("aws_source,f",     po::value<std::string>()->default_value("gen"),         "source of weather info (gen, metar, or uart)")
        ("aws_id,i",         po::value<int>()->default_value(0),                     "set the aws id")
        ("aws_web_page,p",   po::value<std::string>()->default_value("updater.php"), "set the aws web page")
        ("aws_metar_file,m", po::value<std::string>()->default_value("KPNS.TXT"),    "set the metar file location")
        ("aws_devname,n",    po::value<std::string>()->default_value("AWS1"),        "set the device name")
        ("aws_web_server,s", po::value<std::string>()->default_value("localhost"),   "set the aws web server address")
        ("aws_btemp,btemp",  po::value<bool>()->default_value(true),                 "temperature is live data")
        ("aws_bdewp,bdewp",  po::value<bool>()->default_value(true),                 "dewpoint is live data")
        ("aws_bpres,bpres",  po::value<bool>()->default_value(true),                 "pressure is live data")
        ("aws_bvisi,bvisi",  po::value<bool>()->default_value(true),                 "visibility is live data")
        ("aws_bwdir,bwdir",  po::value<bool>()->default_value(true),                 "wind direction is live data")
        ("aws_bwspe,bwspe",  po::value<bool>()->default_value(true),                 "wind speed is live data");

    po::variables_map vm;
    std::ifstream config_file("config.ini", std::ifstream::in);
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::store(po::parse_config_file(config_file, desc), vm);
    po::notify(vm);

    if (vm.count("help")) {
        std::cout << desc << std::endl;
        return 1;
    }

    // Set variables based off provided config values
    boost::unique_lock<boost::shared_mutex> writeLock(smConfig);
    sServer        = vm["aws_web_server"].as<std::string>();
    sPage          = vm["aws_web_page"].as<std::string>();
    iID            = vm["aws_id"].as<int>();
    bDebug         = vm["aws_debug"].as<bool>();
    sSource        = vm["aws_source"].as<std::string>();
    sDevice        = vm["aws_device"].as<std::string>();
    sDevName       = vm["aws_devname"].as<std::string>();
    sMetarFile     = vm["aws_metar_file"].as<std::string>();
    bTemperature   = vm["aws_btemp"].as<bool>();
    bDewPoint      = vm["aws_bdewp"].as<bool>();
    bPressure      = vm["aws_bpres"].as<bool>();
    bVisibility    = vm["aws_bvisi"].as<bool>();
    bWindDirection = vm["aws_bwdir"].as<bool>();
    bWindSpeed     = vm["aws_bwspe"].as<bool>();
    bNeedsMetar    = (sSource == "metar") || !(bTemperature && bDewPoint && bPressure && bVisibility && bWindDirection && bWindSpeed);
    writeLock.unlock();

    // Source and device error checking
    if (!(sSource == "gen" || sSource == "metar" || sSource == "uart"))
    {
        std::cout << "Invalid source. Choose gen, metar, or uart." << std::endl;
        return 1;
    }

    if (sSource == "uart" && sDevice == "unk")
    {
        std::cout << "Device not given, please give a device for the specified source." << std::endl;
        return 1;
    }

    // Check to see if config exists. If not, create it
    std::ifstream fcheck("config.ini");
    if (!fcheck.good())
    {
        fcheck.close();
        writeConfig();
    }
    else
        fcheck.close();

    return 0;
}

// Write config file based on previously set config values
void Config::writeConfig()
{
    std::ofstream fout("config.ini");
    fout << "aws_web_server = " << sServer        << "\n";
    fout << "aws_web_page = "   << sPage          << "\n";
    fout << "aws_id = "         << iID            << "\n";
    fout << "aws_debug = "      << bDebug         << "\n";
    fout << "aws_source = "     << sSource        << "\n";
    fout << "aws_device = "     << sDevice        << "\n";
    fout << "aws_devname = "    << sDevName       << "\n";
    fout << "aws_metar_file = " << sMetarFile     << "\n";
    fout << "aws_btemp = "      << bTemperature   << "\n";
    fout << "aws_bdewp = "      << bDewPoint      << "\n";
    fout << "aws_bpres = "      << bPressure      << "\n";
    fout << "aws_bvisi = "      << bVisibility    << "\n";
    fout << "aws_bwdir = "      << bWindDirection << "\n";
    fout << "aws_bwspe = "      << bWindSpeed     << "\n";
    fout.close();
}

// Listen for config changes
void Config::listenConfig()
{
    while (true)
    {
        conInfoSetup *setupInfo = new conInfoSetup();

        try
        {
            boost::asio::io_service io_service;
            receiver r(io_service, boost::asio::ip::address::from_string("0.0.0.0"),
                       boost::asio::ip::address::from_string("239.255.0.1"));
            r.read(iID, setupInfo);

            // Wait a second to make sure the setup program has time to open the socket
            boost::this_thread::sleep( boost::posix_time::seconds( 1 ) );

            // Block to put things into a lower scope, so they are destroyed at the correct time
            {
                // Open tcp stream to the setup program
                boost::asio::ip::tcp::iostream setupStream(setupInfo->getSetupHost(), setupInfo->getSetupPort());
                if (!setupStream)
                  std::cout << "Unable to connect: " << setupStream.error().message() << std::endl;
                else
                {
                    // Create struct to stream
                    structConfig stConfig;
                    stConfig.sServer  = sServer;
                    stConfig.sPage    = sPage;
                    stConfig.iID      = iID;
                    stConfig.bDebug   = bDebug;
                    stConfig.sSource  = sSource;
                    stConfig.sDevice  = sDevice;
                    stConfig.sDevName = sDevName;

                    // Send the struct
                    boost::archive::text_oarchive archive(setupStream);
                    archive << stConfig;
                    setupStream.flush();
                    setupStream.close();

                    // Wait for response on which device is being configured
                    r.read(iID);
                }
            }

            // If this device is being configured
            if (r.isConfiging())
            {
                structConfig stConfig;

                r.setIsConfiging(false);

                boost::asio::ip::tcp::iostream setupStream(setupInfo->getSetupHost(), setupInfo->getSetupPort());
                boost::archive::text_iarchive ia(setupStream);
                ia >> stConfig;

                boost::unique_lock<boost::shared_mutex> writeLock(smConfig);
                sServer  = stConfig.sServer;
                sPage    = stConfig.sPage;
                iID      = stConfig.iID;
                bDebug   = stConfig.bDebug;
                sSource  = stConfig.sSource;
                sDevice  = stConfig.sDevice;
                sDevName = stConfig.sDevName;
                writeLock.unlock();
                writeConfig();
            }
        }
        catch (boost::thread_interrupted&) {return;}
    }

}

//Stuff for the multicast receiver class
Config::receiver::receiver(boost::asio::io_service& io_service,
                           const boost::asio::ip::address& listen_address,
                           const boost::asio::ip::address& multicast_address)
  : socket_(io_service)
{
  // Create the socket so that multiple may be bound to the same address.
  boost::asio::ip::udp::endpoint listen_endpoint(listen_address, 30001);
  socket_.open(listen_endpoint.protocol());
  socket_.set_option(boost::asio::ip::udp::socket::reuse_address(true));
  socket_.bind(listen_endpoint);

  bIsConfiging = false;

  // Join the multicast group.
  socket_.set_option(boost::asio::ip::multicast::join_group(multicast_address));
}

void Config::receiver::read(int iID, conInfoSetup *setupInfo)
{
  boost::asio::ip::udp::endpoint sender;
  std::vector< char > buffer;
  std::size_t bytes_readable = 0;
  while ( true )
  {
    // Poll until data is available.
    while ( !bytes_readable )
    {
      // Issue command to socket to get number of bytes readable.
      boost::asio::socket_base::bytes_readable num_of_bytes_readable( true );
      socket_.io_control( num_of_bytes_readable );

      // Get the value from the command.
      bytes_readable = num_of_bytes_readable.get();

      // If there is no data available, then sleep.
      if ( !bytes_readable )
      {
        boost::this_thread::sleep( boost::posix_time::seconds( 1 ) );
      }
    }

    // Resize the buffer to store all available data.
    buffer.resize( bytes_readable );

    // Read available data.
    socket_.receive_from(
      boost::asio::buffer( buffer, bytes_readable ),
      sender );

    try
    {
        if (setupInfo != NULL)
        {
            std::istringstream iss(std::string(buffer.begin(), buffer.end()));
            boost::archive::text_iarchive ia(iss);
            ia >> *setupInfo;
        }
        else
            bIsConfiging = (std::string(buffer.begin(), buffer.end()) == std::to_string(iID));

        // Got something useful, break loop to go to next part of setup
        break;
    }
    catch(boost::archive::archive_exception&) { std::cout << "Invalid multicast packet received." << std::endl; }
  }
}
