//
// weatherupdate.cpp
// by Aaron Kling
// ~~~~~~~~~~~~~~~
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include "weatherupdate.h"
#include <iostream>
#include <boost/asio.hpp>

using boost::asio::ip::tcp;

// Send the weather data to the updater web page
int WeatherUpdate::send(WeatherData *data, Config *config)
{
    return !((config->getSource() == "metar") && data->hasNewMetar()) || sendupdate(data, config, getPostString(data, config, false)) ||
           (config->isNeedsMetar() && data->hasNewMetar() && (config->getSource() != "metar") &&
            sendupdate(data, config, getPostString(data, config, true)));
}

int WeatherUpdate::sendupdate(WeatherData *data, Config *config, std::string sPost)
{
    std::cout << "Sending data" << std::endl;

    // 90% of this code is a Boost example. So are the comments.
    // I changed the example to send a POST request instead of a GET. Also, I
    //  manually built the POST string which was more difficult than it should have been.
    try
    {
        boost::shared_lock<boost::shared_mutex> readLock(config->smConfig);

        boost::asio::io_service io_service;

        // Get a list of endpoints corresponding to the server name.
        tcp::resolver resolver(io_service);
        tcp::resolver::query query(config->getServer(), "http");
        tcp::resolver::iterator endpoint_iterator = resolver.resolve(query);
        tcp::resolver::iterator end;

        // Try each endpoint until we successfully establish a connection.
        tcp::socket socket(io_service);
        boost::system::error_code error = boost::asio::error::host_not_found;
        while (error && endpoint_iterator != end)
        {
            socket.close();
            socket.connect(*endpoint_iterator++, error);
        }
        if (error)
            throw boost::system::system_error(error);

        // Form the request. We specify the "Connection: close" header so that the
        // server will close the socket after transmitting the response. This will
        // allow us to treat all data up until the EOF as the content.
        boost::asio::streambuf request;
        std::ostream request_stream(&request);
        request_stream << "POST " << "/" << config->getPage() << " HTTP/1.1\r\n";
        request_stream << "Host: " << config->getServer() << "\r\n";
        request_stream << "Accept: */*\r\n";
        request_stream << "Connection: close\r\n";
        request_stream << "Content-Type: application/x-www-form-urlencoded\r\n";
        request_stream << "Content-Length: " << sPost.length() << "\r\n\r\n";
        request_stream << sPost << "\r\n\r\n";

        readLock.unlock();

        // Send the request.
        boost::asio::write(socket, request);

        // Read the response status line.
        boost::asio::streambuf response;
        boost::asio::read_until(socket, response, "\r\n");

        // Check that response is OK.
        std::istream response_stream(&response);
        std::string http_version;
        response_stream >> http_version;
        unsigned int status_code;
        response_stream >> status_code;
        std::string status_message;
        std::getline(response_stream, status_message);
        if (!response_stream || http_version.substr(0, 5) != "HTTP/")
        {
            if (config->isDebug())
                std::cout << "Invalid response\n";

            return 1;
        }
        if (status_code != 200)
        {
            if (config->isDebug())
                std::cout << "Response returned with status code " << status_code << "\n";

            return 1;
        }

        // Read the response headers, which are terminated by a blank line.
        boost::asio::read_until(socket, response, "\r\n\r\n");

        // Process the response headers.
        std::string header;
        while (std::getline(response_stream, header) && header != "\r") {}

        if (config->isDebug())
        {
            std::cout << "From web server" << std::endl;

            // Write whatever content we already have to output.
            if (response.size() > 0)
                std::cout << &response;

            // Read until EOF, writing data to output as we go.
            while (boost::asio::read(socket, response,
                   boost::asio::transfer_at_least(1), error))
                std::cout << &response;
        }
        else
        {
            while (boost::asio::read(socket, response,
                   boost::asio::transfer_at_least(1), error)) {}
        }

        if (error != boost::asio::error::eof)
            throw boost::system::system_error(error);
    }
    catch (std::exception& e)
    {
        if (config->isDebug())
            std::cout << "Exception: " << e.what() << "\n";
    }

    return 0;
}

std::string WeatherUpdate::getPostString(WeatherData *data, Config *config, bool isMetar)
{
    // I should not have to go to this much trouble to get a 2 precision float as a string...
    int iPressure = (int)round(data->getPressure()*100);
    std::string sFloat = std::to_string(iPressure);
    sFloat.insert((iPressure < 1000 ? 1 : 2), ".");

    std::string sPost;
    sPost = "timerec=" + (isMetar ? data->getMetarTime() : data->getRecTime()) + "&devid=" + std::to_string(config->getID()) +
            "&devname=" + config->getDevName();
    if (isMetar)
    {
        if (!config->isDewPReal()) sPost += "&dpnt=" + std::to_string(data->getDewpoint());
        if (!config->isPresReal()) sPost += "&pres=" + sFloat;
        if (!config->isTempReal()) sPost += "&temp=" + std::to_string(data->getTemperature());
        if (!config->isVisiReal()) sPost += "&vis=" + std::to_string(data->getVisibility());
        if (!config->isWinDReal()) sPost += "&wdir=" + std::to_string(data->getWindDirection());
        if (!config->isWinSReal()) sPost += "&wspd=" + std::to_string(data->getWindSpeed());
    }
    else
    {
        if (config->isDewPReal()) sPost += "&dpnt=" + std::to_string(data->getDewpoint());
        if (config->isPresReal()) sPost += "&pres=" + sFloat;
        if (config->isTempReal()) sPost += "&temp=" + std::to_string(data->getTemperature());
        if (config->isVisiReal()) sPost += "&vis=" + std::to_string(data->getVisibility());
        if (config->isWinDReal()) sPost += "&wdir=" + std::to_string(data->getWindDirection());
        if (config->isWinSReal()) sPost += "&wspd=" + std::to_string(data->getWindSpeed());
    }

    return sPost;
}
