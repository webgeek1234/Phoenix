//
// weatherdata.cpp
// by Aaron Kling
// ~~~~~~~~~~~~~~~
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include "weatherdata.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>
#include <time.h>
#include "boost/date_time/posix_time/posix_time.hpp"
#include "boost/date_time/local_time/local_time.hpp"
#include "boost/algorithm/string.hpp"
extern "C" {
#include "metar/metar.h"
}


namespace pt = boost::posix_time;
using namespace std;

WeatherData::WeatherData(Config *paramConfig)
{
    config = paramConfig;
}

WeatherData::~WeatherData()
{
    // If pulling real data, disconnect from the microprocessor
    if (config->getSource() == "uart")
        destructUART();
}

// Initialize the weather data class
int WeatherData::initialize()
{
    bHasNewMetar = false;

    if (config->getSource() == "gen")
        srand(time(NULL));
    else if (config->getSource() == "uart")
    {
        if (initializeUART())
        {
            if (config->isDebug())
                cout << "There was an error initializing UART." << endl;

            return 1;
        }
    }

    return 0;
}

// Set up the UART connection
int WeatherData::initializeUART()
{
    ssWeather = new SimpleSerial(config->getDevice(), 576000);

    // If we got here, opening the device was successful
    return 0;
}

// Destroy the UART connection
void WeatherData::destructUART()
{
    delete ssWeather;
}

// Get a set of weather values
void WeatherData::updateData()
{
    Decoded_METAR *mData = new Decoded_METAR();

    if (config->isNeedsMetar())
    {
        // Get Metar Data
        bHasNewMetar = false;
        std::string strNewMTime;
        std::ifstream fMetar(config->getMetarLoc());
        if (fMetar.good())
        {
            getline(fMetar, strNewMTime);
            if (strNewMTime != "" && sMetarTime != strNewMTime)
            {
                std::string strMetar;
                sMetarTime = strNewMTime;
                bHasNewMetar = true;
                getline(fMetar, strMetar);
                fMetar.close();
                std::vector<char> chars(strMetar.c_str(), strMetar.c_str() + strMetar.size() + 1u);
                DcdMETAR(&(chars[0]), mData);
            }
            else
                fMetar.close();
        }
        else
        {
            fMetar.close();
            std::cout << "Failed to open METAR file." << std::endl;
        }
    }

    if (config->getSource() == "gen")
    {
        // Do random number generation and get current date/time
        iTemperature   = (config->isTempReal() ? (rand()%140)-20 : (mData->temp * 9.0 / 5.0) + 32);
        iDewPoint      = (config->isDewPReal() ? (rand()%140)-20 : (mData->dew_pt_temp * 9.0 / 5.0) + 32);
        fPressure      = (config->isPresReal() ? (float)(rand()%200+2900)/100 : mData->inches_altstng);
        iVisibility    = (config->isVisiReal() ? (rand()%10) : round(mData->prevail_vsbySM));
        iWindDirection = (config->isWinDReal() ? (rand()%360) : mData->winData.windDir);
        iWindSpeed     = (config->isWinSReal() ? (rand()%174) : mData->winData.windSpeed);
        sTime          = to_iso_string(pt::second_clock::local_time()).erase(8,1);
    }
    else if (config->getSource() == "uart")
    {
        // Pull from UART
        std::vector<std::string> vWeatherValues;
        std::string sWeatherValues = ssWeather->readLine();
        sTime = to_iso_string(pt::second_clock::local_time()).erase(8,1);
        boost::split(vWeatherValues, sWeatherValues, boost::is_any_of(";"));

        iTemperature   = (config->isTempReal() ? stoi(vWeatherValues[1]) : (mData->temp * 9.0 / 5.0) + 32);
        iDewPoint      = (config->isDewPReal() ? stoi(vWeatherValues[0]) : (mData->dew_pt_temp * 9.0 / 5.0) + 32);
        fPressure      = (config->isPresReal() ? stoi(vWeatherValues[5]) : mData->inches_altstng);
        iVisibility    = (config->isVisiReal() ? stoi(vWeatherValues[2]) : round(mData->prevail_vsbySM));
        iWindDirection = (config->isWinDReal() ? stoi(vWeatherValues[3]) : mData->winData.windDir);
        iWindSpeed     = (config->isWinSReal() ? stoi(vWeatherValues[4]) : mData->winData.windSpeed);
    }
    else if (config->getSource() == "metar" && hasNewMetar())
    {
        sTime = getMetarTime();
        iTemperature   = (mData->temp * 9.0 / 5.0) + 32;
        iDewPoint      = (mData->dew_pt_temp * 9.0 / 5.0) + 32;
        fPressure      = mData->inches_altstng;
        iVisibility    = round(mData->prevail_vsbySM);
        iWindDirection = mData->winData.windDir;
        iWindSpeed     = mData->winData.windSpeed;
    }
}

std::string WeatherData::getMetarTime()
{
    boost::local_time::time_zone_ptr src_zone(new boost::local_time::posix_time_zone("IST"));
    boost::local_time::time_zone_ptr dst_zone(new boost::local_time::posix_time_zone("IST-5"));
    boost::local_time::local_date_time trd_time(boost::posix_time::ptime(boost::gregorian::date(stoi(sMetarTime.substr(0,4)), stoi(sMetarTime.substr(5,2)), stoi(sMetarTime.substr(8,2))),
                             boost::posix_time::time_duration(stoi(sMetarTime.substr(11,2)), stoi(sMetarTime.substr(14,2)), 0, 0)),
                             src_zone);

    return to_iso_string(trd_time.local_time_in(dst_zone).local_time()).erase(8,1);
}
