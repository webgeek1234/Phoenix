//
// main.cpp (for AWS project)
// by Aaron Kling
// ~~~~~~~~~~~~~~~
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//

#include "weatherdata.h"
#include "config.h"
#include "weatherupdate.h"
#include <iostream>
#include <boost/thread.hpp>

void weather_loop(WeatherData *data, Config *config, WeatherUpdate *update);

int main(int argc, char** argv)
{
    // Declare classes
    WeatherData   *data;
    Config        *config;
    WeatherUpdate *update;

    // Initialize the config class and set all settings
    config = new Config();
    if (config->initialize(argc, argv))
        return 1;

    // Initialize the data and update classes
    data = new WeatherData(config);
    if (data->initialize())
        return 1;

    update = new WeatherUpdate();

    // Split a thread to listen for config changes
    boost::thread thCListen(&Config::listenConfig, config);

    // Split a thread to handle the main weather loop.
    boost::thread thWLoop(weather_loop, data, config, update);
    thWLoop.join(); // Main function will stop here for normal operation

    // If exiting, we want to interrupt the config change listening thread
    thCListen.interrupt();
    thCListen.join();

    // Clean up classes in appropriate order
    delete update;
    delete data;
    delete config;

    return 0;
}

// Function to drive the gathering and sending of the weather values
void weather_loop(WeatherData *data, Config *config, WeatherUpdate *update)
{
    bool boolRun = true;

    while (boolRun)
    {
        try
        {
            // Wait for a new set of weather values
            data->updateData();

            if (config->isDebug())
            {
                std::cout << "From Driver" << std::endl;
                std::cout << "Time Received:\t" << data->getRecTime() << std::endl;
                std::cout << "Dewpoint:\t" << data->getDewpoint() << " F" << std::endl;
                std::cout << "Pressure:\t" << data->getPressure() << " inHg" << std::endl;
                std::cout << "Temperature:\t" << data->getTemperature() << " F" << std::endl;
                std::cout << "Visibility:\t" << data->getVisibility() << " Nautical Miles" << std::endl;
                std::cout << "Wind Direction:\t" << data->getWindDirection() << " Deg" << std::endl;
                std::cout << "Wind Speed:\t" << data->getWindSpeed() << " Knots" << std::endl << std::endl;
            }

            // Send the weather values to the updater web page, then wait 45 seconds before nexxt reading
            update->send(data, config);
            boost::this_thread::sleep(boost::posix_time::seconds(45));
        }
        catch (boost::thread_interrupted&) { boolRun = false; }
    }
}
